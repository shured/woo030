#!/usr/bin/perl
############################################
### Sync local db met WOO vanaf de website
## van de gemeente Utrecht
##
## (C) NONE - Sharing is caring
##
## 2023-09-02 - Eerste versie
############################################

use LWP::UserAgent;
use DBI;
use Data::Dumper;
use IO::Handle;
use Data::Validate::URI;

## Connect to DB
my $dbname="woo030.db";
my $dbh = DBI->connect("dbi:SQLite:dbname=$dbname","","") or die $DBI::errstr;

open(RUNVERSLAG,">", "Runverslag.txt") or die $!;

## Clean old run
my $delete  = "delete from WOO;";
$sth = $dbh->prepare($delete) or die $dbh->errstr;
$sth->execute() or die $dbh->errstr;


require HTTP::Cookies;

my $ua = new LWP::UserAgent;
$ua->agent('OpenWOOBot/1.0');
$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME}=0;
$ua->ssl_opts( verify_hostname => 0 ,SSL_verify_mode => 0x00);

$| = 1;

# Clear the old updates table
my $delete = "delete from updates;";
$sth = $dbh->prepare($delete) or die $dbh->errstr;
$sth->execute() or die $dbh->errstr;

sub WOB
{
   $WOB = $_[0];
   my $req = new HTTP::Request('GET',$WOB);
   my $res = $ua->request($req);
   my $content=$res->content;
   my $H1_pos1=index($content,"<h1>")+4;
   my $H1_pos2=index($content,"</h1>");
   my $H1 = substr($content,$H1_pos1,($H1_pos2-$H1_pos1));
   print "HI => $H1\n";
   my $WOB_NR = substr($H1,0,9);
   my $WOB_DESCR = substr($H1,9);
   $WOB_DESCR =~ s/Woo-besluit over //g;
   $WOB_DESCR =~ s/Wob-besluit over //g;
   $WOB_DESCR =~ s/Woo-verzoek over //g;
   $WOB_DESCR =~ s/Wob-verzoek over //g;
   print RUNVERSLAG "Wob = $WOB_NR - $WOB_DESCR\n";
   my $insert = "insert into updates ('WOO_ID','WOO_Descr','WOO_URI') values(\"$WOB_NR\",\"$WOB_DESCR\",\"$WOB\");";
   $sth = $dbh->prepare($insert) or die $dbh->errstr;
   $sth->execute() or die $dbh->errstr;

}


my $Main_URL = "https://www.utrecht.nl/bestuur-en-organisatie/publicaties/openbaar-gemaakte-informatie-na-wob-en-woo-verzoeken/";
my $page = 1;
my $uri = $Main_URL;

while ( $page < 30 )
{
  print "URL $uri\n";
  my $req = new HTTP::Request('GET',$uri);

  my $res = $ua->request($req);
  my $content=$res->content;
  my $positie = index($content,'href');
  my $prefix = "://www.utrecht.nl/bestuur-en-organisatie/publicaties/openbaar-gemaakte-informatie-na-wob-verzoeken/wob-verzoek/2023"; 
  while ( $positie > 0 ) 
    {
       $content = substr($content,$positie+6);
       if (substr($content,0,121) =~ m/$prefix/ )
       {
       my $tag = index($content,'">2023');
       my $WOB_DESCR = substr($content,0,$tag);
       if (index($WOB_DESCR,'datetime') != -1) { 
             $tag=index($content,'">2022');
             $WOB_DESCR = substr($content,0,$tag);
       }
       my $WOB_URI = $WOB_DESCR;
       my $uriValidator = new Data::Validate::URI();
       $WOB_DESCR =~ s/https:\/\/www.utrecht.nl\/bestuur\-en\-organisatie\/publicaties\/openbaar-gemaakte-informatie-na-wob-verzoeken\/wob-verzoek\///g;
       $WOB_DESCR =~ s/https:\/\/www.utrecht.nl\/bestuur\-en\-organisatie\/publicaties\/openbaar-gemaakte-informatie-na-wob-verzoeken\/woo besluit\///g;
       my $WOB_NR = substr($WOB_DESCR,0,9);
       $WOB_DESCR = substr($WOB_DESCR,10);
       $WOB_DESCR =~ s/woo-besluit-over-//;
       $WOB_DESCR =~ s/woo-besluit-//;
       $WOB_DESCR =~ s/woo-verzoek-//;
       $WOB_DESCR =~ s/-/ /g;
       $WOB_DESCR =~ s/\// /g;
       print "Wob = $WOB_NR - $WOB_DESCR\n";
       print RUNVERSLAG "Wob = $WOB_NR - $WOB_DESCR\n";
       my $insert = "insert into updates ('WOO_ID','WOO_Descr','WOO_URI') values(\"$WOB_NR\",\"$WOB_DESCR\",\"$WOB_URI\");";
       $sth = $dbh->prepare($insert) or die $dbh->errstr;
       $sth->execute() or die $dbh->errstr;

       #WOB ($found) if $uriValidator->is_uri($found)
   }
      $positie = index($content,'href');
  }
  $page++;
  $uri=$Main_URL."page/".$page."/";
  print "Nieuwe Uri = $uri\n";

}

$sth->finish() or die $dbh->errstr; 


