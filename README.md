Ooit was er openwob.nl.

Daarop stond een doorzoekbaar archief van alle WOB-verzoeken en bijbehorende
documenten van de gemeente Utrecht. En al hoewel de gemeente nog steeds actief
naar openwob.nl verwijst, is zij al jaren niet meer actief.

Mijn pogingen om openwob.nl nieuw leven in te blazen zijn gefaald. De ooit zo transparante gemeente
Utrecht is verworden tot een bastion van tegenwerking waar ze alleen nog het wettelijke minimum doen
aan transparantie en openbaarheid.

Daarom is er nu openwoo.nl. In deze git staan de scripts waarmee de database van openwoo.nl gemaakt wordt.

Op deze site www.openwoo.nl vind je een doorzoekbaar archief van alle wob/woo van de gemeente Utrecht.
Waar mogelijk deeplinken de zoek-resultaten door naar de onvindbare pagina op de gemeentelijke website.
